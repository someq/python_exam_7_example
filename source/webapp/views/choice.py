from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView, UpdateView, DeleteView

from webapp.models import Poll, Choice
from webapp.forms import ChoiceForm


class ChoiceCreateView(CreateView):
    model = Choice
    form_class = ChoiceForm
    template_name = 'choice/create.html'

    def get_context_data(self, **kwargs):
        poll = get_object_or_404(Poll, pk=self.kwargs.get('pk'))
        kwargs['poll'] = poll
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        poll = get_object_or_404(Poll, pk=self.kwargs.get('pk'))
        form.instance.poll = poll
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('poll_detail', kwargs={'pk': self.kwargs.get('pk')})


class ChoiceUpdateView(UpdateView):
    model = Choice
    form_class = ChoiceForm
    template_name = 'choice/update.html'
    context_object_name = 'choice'

    def get_success_url(self):
        return reverse('poll_detail', kwargs={'pk': self.object.poll.pk})


class ChoiceDeleteView(DeleteView):
    model = Choice
    context_object_name = 'choice'
    template_name = 'choice/delete.html'

    def get_success_url(self):
        return reverse('poll_detail', kwargs={'pk': self.object.poll.pk})
