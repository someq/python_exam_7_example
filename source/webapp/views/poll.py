from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from webapp.models import Poll
from webapp.forms import PollForm


class IndexView(ListView):
    model = Poll
    template_name = 'poll/index.html'
    ordering = ['-created_at']
    paginate_by = 5
    paginate_orphans = 2
    context_object_name = 'polls'


class PollView(DetailView):
    model = Poll
    template_name = 'poll/poll.html'
    context_object_name = 'poll'


class PollCreateView(CreateView):
    model = Poll
    form_class = PollForm
    template_name = 'poll/create.html'

    def get_success_url(self):
        return reverse('poll_detail', kwargs={'pk': self.object.pk})


class PollUpdateView(UpdateView):
    model = Poll
    form_class = PollForm
    context_object_name = 'poll'
    template_name = 'poll/update.html'

    def get_success_url(self):
        return reverse('poll_detail', kwargs={'pk': self.object.pk})


class PollDeleteView(DeleteView):
    model = Poll
    context_object_name = 'poll'
    template_name = 'poll/delete.html'

    def get_success_url(self):
        return reverse('index')
