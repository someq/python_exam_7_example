from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import View

from webapp.models import Poll, Answer


class PollAnswerView(View):
    def get(self, request, *args, **kwargs):
        poll = get_object_or_404(Poll, pk=kwargs.get('pk'))
        return render(request, 'answer/answer.html', context={'poll': poll})

    def post(self, request, *args, **kwargs):
        poll_pk = kwargs.get('pk')
        poll = get_object_or_404(Poll, pk=poll_pk)
        choice_pk = request.POST.get('choice_pk')
        choice = get_object_or_404(poll.choices, pk=choice_pk)
        Answer.objects.create(poll=poll, choice=choice)
        return redirect('poll_stats', pk=poll_pk)


class PollStatsView(View):
    def get(self, request, *args, **kwargs):
        poll = get_object_or_404(Poll, pk=kwargs.get('pk'))
        total = poll.answers.count()
        results = []
        colors = ['#F00', '#0F0', '#00F', '#FF0', '#0FF', '#F0F', '#FFF', '#000']
        for choice_id, choice in enumerate(poll.choices.all()):
            count = choice.answers.count()
            percent = round(count / total * 100, 2) if total else 0.0
            text = choice.text
            color = colors[choice_id % len(colors)]
            results.append({'count': count, 'percent': percent, 'text': text, 'color': color})

        return render(request, 'answer/stats.html', context={'poll': poll, 'results': results})
