from django.contrib import admin
from webapp.models import Poll, Choice, Answer


class ChoiceInline(admin.TabularInline):
    model = Choice
    fields = ['text']


class PollAdmin(admin.ModelAdmin):
    exclude = []
    readonly_fields = ['created_at']
    inlines = [ChoiceInline]


admin.site.register(Poll, PollAdmin)
admin.site.register(Answer)
